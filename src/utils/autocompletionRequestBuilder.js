const autocompletionRequestBuilder = (autocompletionRequest) => {
  const res = { ...autocompletionRequest };

  if (autocompletionRequest.bounds) {
    res.bounds = new google.maps.LatLngBounds(...autocompletionRequest.bounds);
  }

  if (autocompletionRequest.location) {
    res.location = new google.maps.LatLng(autocompletionRequest.location);
  }

  if (autocompletionRequest.strictBounds) {
    res.strictBounds=true
  } 

  return res;
};

export default autocompletionRequestBuilder;
